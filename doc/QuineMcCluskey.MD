# Quine McCluskey Algorithm

Use `QuantrBooleanAlgebra.qmOptimise` to optimize boolean expressions using [`Quine-McCluskey Algorithm`](https://en.wikipedia.org/wiki/Quine%E2%80%93McCluskey_algorithm).

## Usage

### Sample Code:
```java
String expr = "o=ab+b'";
MyListener listener1 = QuantrBooleanAlgebra.parse(expr);
System.out.println(listener1.output + "=" + listener1.output.getExpr());
MyListener listener2 = QuantrBooleanAlgebra.qmOptimise(listener1);
System.out.println(listener2.output + "=" + listener2.output.getExpr());
```
### Sample Output:
```
o=ab+b'
o=b'+a
```