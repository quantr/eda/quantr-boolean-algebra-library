/*
 * Copyright 2024 ronald.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import static hk.quantr.algebralib.QuantrBooleanAlgebra.parseIte;
//import static hk.quantr.algebralib.QuantrBooleanAlgebra.parseIte1;
import javax.swing.*;
import java.io.File;

/**
 *
 * @author ronald
 */
public class GraphPanel extends JLabel {

	GraphViz gv;

//	public static String convertIteToDot(String ite, String order) {
//		String dot = "digraph BDD {\n";
//		IteListener l = parseIte(ite, order);
//		dot += l.getDotGraph();
//		if (dot.contains("->1")) {
//			dot += "T[shape=\"box\"];\n";
//		}
//		if (dot.contains("->0")) {
//			dot += "F[shape=\"box\"];\n";
//		}
//		dot = dot.replaceAll("1", "T").replaceAll("0", "F").replaceAll("\\(", "6").replaceAll("\\)", "9").replaceAll(",", "_").replaceAll("\\*", "");
//		dot += "}";
//		System.out.println(dot);
//		return dot;
//	}

	public static String convertIteToDot(String ite, String order) {
		/*
		String dot = "digraph BDD {\n";
		IteListener l = parseIte(ite);
		dot += l.getDotGraph();
		if (dot.contains("->1")) {
			dot += "T[shape=\"box\"];\n";
		}
		if (dot.contains("->0")) {
			dot += "F[shape=\"box\"];\n";
		}
		dot = dot.replaceAll("1", "T").replaceAll("0", "F").replaceAll("\\(", "6").replaceAll("\\)", "9").replaceAll(",", "_").replaceAll("\\*", "");
		dot += "}";
		 */
		String dot = parseIte(ite, order).getDot();
		// System.out.println(dot);
		return dot;
	}

//	public void loadDot(String dot, String fileName) {
//		gv = new GraphViz();
////		gv.addln(gv.start_graph());
//		gv.add(dot);
////		gv.addln(gv.end_graph());
//		String type = "png";
////		gv.decreaseDpi();
//		File out = new File(fileName + "." + type);
//		gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type), out);
//		this.setIcon(new ImageIcon(fileName + "." + type));
//	}

	public void loadDot(String dot, String fileName) {
		gv = new GraphViz();
//		gv.addln(gv.start_graph());
		gv.add(dot);
//		gv.addln(gv.end_graph());
		String type = "png";
//		gv.decreaseDpi();
		File out = new File(fileName + "." + type);
		gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type), out);
		this.setIcon(new ImageIcon(fileName + "." + type));
	}
}
