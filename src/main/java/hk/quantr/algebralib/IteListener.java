/*
 * Copyright 2024 ronald.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import hk.quantr.algebralib.antlr.IteParser;
import hk.quantr.algebralib.antlr.IteParserBaseListener;
import hk.quantr.algebralib.data.Ite;
import hk.quantr.algebralib.data.Terminal;
import hk.quantr.algebralib.data.Variable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author ronald
 */
public class IteListener extends IteParserBaseListener {

	private static Map<String, Ite> nodes;

	public IteListener() {
		nodes = new HashMap<>();
	}

	@Override
	public void exitIte(IteParser.IteContext ctx) {
		Ite ite = buildIte(ctx);
//		if (ctx.getText().equals("ITE(ITE(a,ITE(b,1,0),0),1,ITE(c,1,0))")) {
//			System.out.println("break");
//		}
		// System.out.println(ite.toString());
		if (ite.isIte()) {
			// System.out.println("test: " + ite.toString());
			addNode(ite.getMasterIte());
			ite.update(1, ite.getTrueExpression(), false);
			ite.update(0, ite.getFalseExpression(), false);

			// System.out.println("\n" + dotGraph.toString());
		}
	}

	public boolean addNode(Ite ite) {
		if (!nodes.keySet().contains(ite.getMasterIte().toString()) && ite.isIte() && !ite.getCondition().isIte()) {
			System.out.println("add: " + ite);
//			dotGraph.append(ctx.getText()).append("[label=").append(ctx.getText().charAt(ctx.getText().indexOf(",") - 1)).append("];\n");
			nodes.put(ite.toString(), ite);
			return true;
		}
		return false;
	}

	public static Ite buildIte(IteParser.IteContext ctx) {
		Ite ite = new Ite(new Variable("error"));
		if (nodes.containsKey(ctx.getText())) {
			return nodes.get(ctx.getText());
		}
		if (ctx.getText().startsWith("1") || ctx.getText().startsWith("0")) {
			ite = new Ite(new Terminal(Integer.parseInt(ctx.getText())));
			ite.getTerminal().setParent(ite);
		} else if (ctx.getText().length() == 1) {
			// System.out.println("haha: " + ctx.getText());
			ite = new Ite(new Variable(ctx.getText()));
			ite.getVariable().setParent(ite);
		} else {
			// System.out.println("qwq: " + ctx.getText());
			if (ctx.getText().startsWith("ITE")) {
				System.out.println("ahhh: " + buildIte((IteParser.IteContext) ctx.getChild(2))
						+ buildIte((IteParser.IteContext) ctx.getChild(4)) + buildIte((IteParser.IteContext) ctx.getChild(6)));
				ite = new Ite(buildIte((IteParser.IteContext) ctx.getChild(2)),
						buildIte((IteParser.IteContext) ctx.getChild(4)),
						buildIte((IteParser.IteContext) ctx.getChild(6)));
				ite.getCondition().setParent(ite);
				ite.getTrueExpression().setParent(ite);
				ite.getFalseExpression().setParent(ite);

			}
		}
		return ite;
	}

	public static IteParser.IteContext getMasterIf(IteParser.IteContext ctx) {
		try {
			if (!ctx.getChild(2).getText().startsWith("ITE")) {
				return ctx;
			}
			return getMasterIf((IteParser.IteContext) ctx.getChild(2));
		} catch (Exception e) {
			return ctx;
		}
	}

	public String getDot() {
		String dot = "digraph bdd {\n";
		Iterator<Ite> iterator = nodes.values().iterator();
		while (iterator.hasNext()) {
			Ite ite = iterator.next();
			String d = ite.toDot();
			if (!dot.contains(d)) {
				dot += d;
			} else {
				iterator.remove();
			}
		}
		if (dot.contains("->1")) {
			dot += "T[shape=\"box\"];\n";
		}
		if (dot.contains("->0")) {
			dot += "F[shape =\"box\"];\n";
		}
		dot += "}";
		for (Ite ite : nodes.values()) {
			if (ite.getMasterIte().getCondition().toString().equals("a")) {
				System.out.println(ite.toString() + "\n");
				break;
			}
		}
		System.out.println(dot);
		return dot.replaceAll("1", "T").replaceAll("0", "F")
				.replaceAll("\\(", "6").replaceAll("\\)", "9")
				.replaceAll(",", "_");
	}
}
