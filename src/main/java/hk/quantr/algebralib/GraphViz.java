/*
 * Copyright 2024 ronald.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.Properties;

/**
 *
 * @author ronald
 */
public class GraphViz {

	private static String osName = System.getProperty("os.name").replaceAll("\\s", "").replaceAll("\\d", "");

	private final static String cfgProp = "config.properties";
	private final static Properties configFile = new Properties() {
		private final static long serialVersionUID = 1L;

		{
			try {
				load(new FileInputStream(cfgProp));
			} catch (Exception e) {
			}
		}
	};

	private static String TEMP_DIR = System.getProperty("java.io.tmpdir");// "/tmp";

	private static String DOT = configFile.getProperty("dotFor" + osName);

	private int dpi = 80;

	private StringBuilder graph = new StringBuilder();

	public GraphViz() {
	}
	
	public String getDotSource() {
		return this.graph.toString();
	}

	public void add(String line) {
		this.graph.append(line);
	}

	public byte[] getGraph(String dot_source, String type) {
		File dot;
		byte[] img_stream = null;

		try {
			dot = writeDotSourceToFile(dot_source);
			if (dot != null) {
				img_stream = get_img_stream(dot, type);
				if (dot.delete() == false) {
					System.err.println("Warning: " + dot.getAbsolutePath() + " could not be deleted!");
				}
				return img_stream;
			}
			return null;
		} catch (java.io.IOException ioe) {
			return null;
		}
	}

	public int writeGraphToFile(byte[] img, File to) {
		try {
			FileOutputStream fos = new FileOutputStream(to);
			fos.write(img);
			fos.close();
		} catch (java.io.IOException ioe) {
			return -1;
		}
		return 1;
	}

	private byte[] get_img_stream(File dot, String type) {
		File img;
		byte[] img_stream = null;

		try {
			img = File.createTempFile("graph_", "." + type, new File(GraphViz.TEMP_DIR));
			Runtime rt = Runtime.getRuntime();

			// patch by Mike Chenault
			String[] args = {DOT, "-T" + type, "-Gdpi=" + dpi, dot.getAbsolutePath(), "-o", img.getAbsolutePath()};
			Process p = rt.exec(args);

			p.waitFor();

			FileInputStream in = new FileInputStream(img.getAbsolutePath());
			img_stream = new byte[in.available()];
			in.read(img_stream);
			// Close it if we need to
			if (in != null) {
				in.close();
			}

			if (img.delete() == false) {
				System.err.println("Warning: " + img.getAbsolutePath() + " could not be deleted!");
			}
		} catch (java.io.IOException ioe) {
			System.err.println("Error:    in I/O processing of tempfile in dir " + GraphViz.TEMP_DIR + "\n");
			System.err.println("       or in calling external command");
			ioe.printStackTrace();
		} catch (java.lang.InterruptedException ie) {
			System.err.println("Error: the execution of the external program was interrupted");
			ie.printStackTrace();
		}

		return img_stream;
	}

	private File writeDotSourceToFile(String str) throws java.io.IOException {
		File temp;
		try {
			temp = File.createTempFile("dorrr", ".dot", new File(GraphViz.TEMP_DIR));
			FileWriter fout = new FileWriter(temp);
			fout.write(str);
			BufferedWriter br = new BufferedWriter(new FileWriter("dotsource.dot"));
			br.write(str);
			br.flush();
			br.close();
			fout.close();
		} catch (Exception e) {
			System.err.println("Error: I/O error while writing the dot source to temp file!");
			return null;
		}
		return temp;
	}
}
