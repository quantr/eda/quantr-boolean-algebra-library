package hk.quantr.algebralib.data;

import hk.quantr.algebralib.QuantrBooleanAlgebra;
import java.util.stream.Collectors;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class And extends BooleanData {

	public And() {
		super("AND");
	}

	@Override
	public String toString() {
		return (hasBracket ? "(" : "") + children.stream()
				.map(BooleanData::toString)
				.collect(Collectors.joining("")) + (hasBracket ? ")" : "");
	}

	@Override
	public String toBddString() {
		return "and(" + children.stream()
				.map(BooleanData::toBddString)
				.collect(Collectors.joining(",")) + ')';
	}

	@Override
	public String toMdString() {
		return (hasBracket ? "(" : "") + children.stream()
				.map(BooleanData::toMdString)
				.collect(Collectors.joining("")) + (hasBracket ? ")" : "");
	}

	@Override
	public String toIteString(int i, boolean isNot, boolean isFirst) {
		if (i == children.size() - 1) {
			return children.get(i).toIteString(0, isNot, false);
		}
		return "ITE(" + children.get(i).toIteString(0, false, true) + "," + this.toIteString(i + 1, isNot, false) + (isNot ? ",1)" : ",0)");
	}

	@Override
	public boolean eval() {
		for (BooleanData b : children) {
			if (!b.eval()) {
				result = false;
				return false;
			}
		}
		result = true;
		return true;
	}

	@Override
	public BooleanData clone() {
		And and = new And();
		and.hasBracket = this.hasBracket;
		for (BooleanData child : children) {
			and.children.add(child.clone());
		}
		return and;
	}

	@Override
	public BooleanData optimise(QuantrBooleanAlgebra.Law... laws) {
		int _i = -1;
		for (BooleanData child : this.children) {
			this.children.set(++_i, child.optimise(laws));
//			System.out.println(this.children.get(_i).toString() + " [" + this.toString() + "]");
		}
		for (QuantrBooleanAlgebra.Law law : laws) {
			BooleanData _this = this.clone();
			switch (law) {
				case ASSOCIATIVE -> {
					And and = new And();
					for (BooleanData child : _this.children) {
						if (child instanceof And) {
							for (BooleanData c : child.children) {
								and.children.add(c);
								and.hasOptimised = true;
							}
						} else {
							and.children.add(child);
						}
					}
					if (and.hasOptimised) {
						_this = and;
					}
					break;
				}
				case DISTRIBUTIVE -> {
					// not supported yet
					for (BooleanData child : _this.children) {
						if (child instanceof Or) {
							Or or = new Or();
							for (BooleanData c : child.children) {
								And and = new And();
								for (BooleanData child2 : _this.children) {
									if (!child.equals(child2)) {
										if (child2 instanceof And) {
											for (BooleanData subChild2 : child2.children) {
												and.children.add(subChild2);
											}
										} else {
											and.children.add(child2);
										}
										if (child2 instanceof Or) {
											child2.hasBracket = true;
										}
									}
								}
								if (c instanceof And) {
									for (BooleanData subChild2 : c.children) {
										and.children.add(subChild2);
									}
								} else {
									and.children.add(c);
								}
								or.children.add(and);
							}
							_this = or;
							_this.hasBracket = true;
							_this.hasOptimised = true;
							break;
						}
					}
					break;
				}
				case IDENTITY -> {
					And and = new And();
					for (int i = 0; i < _this.children.size(); i++) {
						if (_this.children.get(i) instanceof True) {
							and.hasOptimised = true;
						} else {
							and.children.add(_this.children.get(i));
						}
					}
					if (and.hasOptimised) {
						_this = and;
					}
					break;
				}
				case NEGATION -> {
					for (BooleanData child : _this.children) {
						for (BooleanData child2 : _this.children) {
							if (child.isNegationOf(child2)) {
								_this = new False();
								_this.hasOptimised = true;
								break;
							}
						}
					}
					break;
				}
				case IDEMPOTENT -> {
					And and = new And();
					for (BooleanData child : _this.children) {
						if (and.hasChild(child) == -1) {
							and.children.add(child);
						}
					}
					if (!and.equals(_this)) {
						_this = and;
						_this.hasOptimised = true;
					}
					break;
				}
				case UNIVERSAL_BOUND -> {
					for (BooleanData child : _this.children) {
						if (child instanceof False) {
							_this = new False();
							_this.hasOptimised = true;
							break;
						}
					}
					break;
				}
				case ABSORPTION -> {
					And and = new And();
					for (BooleanData subChild : _this.children) {
						and.children.add(subChild);
						for (BooleanData child : _this.children) {
							if (child instanceof Or) {
								if (!child.equals(subChild) && child.hasChild(subChild) == -1) {
									and.children.add(child);
								} else if (!child.equals(subChild)) {
									and.hasOptimised = true;
								}
							} else if (!child.equals(subChild)) {
								and.children.add(child);
							}
						}
						if (and.hasOptimised) {
							_this = and;
							break;
						}
						and.children.clear();
					}
					break;
				}
				default -> {
//					_this.hasOptimised = false;
				}
			}
			if (!this.equals(_this)) {
//				System.out.println(this.toString() + " --> " + _this.toString() + " (AND." + law.toString() + ")");
//				QuantrBooleanAlgebra.dump(_this);
				if (_this.children.size() == 1 && !(_this instanceof Not)) {
					return _this.children.get(0);
				}
				return _this;
			}
		}
		return this;
	}
}
