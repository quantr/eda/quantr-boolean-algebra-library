/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib.data;

import hk.quantr.algebralib.QuantrBooleanAlgebra;
import java.util.stream.Collectors;

/**
 *
 * @author Ronald Park <pkc6krt22@gmail.com>
 */
public class False extends BooleanData{
	
	public False() {
		super("FALSE");
	}
	
	@Override
	public String toString() {
		return "0";
	}

	@Override
	public String toBddString() {
		return "f";
	}

	@Override
	public String toMdString() {
		return "0";
	}

	@Override
	public String toIteString(int i, boolean isNot, boolean isFirst) {
		return "0";
	}
	
	@Override
	public boolean eval() {
		return false;
	}

	@Override
	public BooleanData clone() {
		False f = new False();
		f.hasBracket = this.hasBracket;
		return f;
	}
	
	@Override
	public BooleanData optimise(QuantrBooleanAlgebra.Law... laws) {
		return this;
	}
}
