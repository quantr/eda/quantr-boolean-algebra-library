package hk.quantr.algebralib.data;

import hk.quantr.algebralib.QuantrBooleanAlgebra;
import static hk.quantr.algebralib.QuantrBooleanAlgebra.Law.DE_MORGANS;
import java.util.stream.Collectors;

/**
 *
 * @author Ronald Park <pkc6krt22@gmail.com>
 */
public class Not extends BooleanData {

	public Not() {
		super("NOT");
	}

	@Override
	public String toString() {
		return (hasBracket ? "(" : "") + this.children.stream()
				.map(BooleanData::toString)
				.collect(Collectors.joining("")) + "\'" + (hasBracket ? ")" : "");
	}

	@Override
	public String toBddString() {
		return "not(" + children.stream()
				.map(BooleanData::toBddString)
				.collect(Collectors.joining(",")) + ')';
	}

	@Override
	public String toMdString() {
		return "\\overline{" + children.stream()
				.map(BooleanData::toMdString)
				.collect(Collectors.joining(",")) + '}';
	}

	@Override
	public String toIteString(int i, boolean isNot, boolean isFirst) {
		return children.get(0).toIteString(0, !isNot, false);
	}

	@Override
	public boolean eval() {
		result = !this.children.get(0).eval();
		return result;
	}

	@Override
	public BooleanData clone() {
		Not not = new Not();
		not.hasBracket = this.hasBracket;
		not.children.add(this.children.get(0).clone());
		return not;
	}

	@Override
	public BooleanData optimise(QuantrBooleanAlgebra.Law... laws) {
		this.children.set(0, this.children.get(0).optimise(laws));
		BooleanData _this = this.clone();
		for (QuantrBooleanAlgebra.Law law : laws) {
			_this = this.clone();
			switch (law) {
				case DOUBLE_NEGATIVE -> {
					int cnt = 1;
					BooleanData b = _this.children.get(0);
					while (b instanceof Not) {
						b = b.children.get(0);
						cnt++;
					}
					if (cnt % 2 == 0) {
						_this = b;
						_this.hasOptimised = true;
					} else if (cnt > 2) {
						Not not = new Not();
						not.children.add(b);
						_this = not;
						_this.hasOptimised = true;
					}
					break;
				}
				case DE_MORGANS -> {
					if (_this.children.get(0) instanceof And) {
						Or or = new Or();
						or.hasBracket = true;
						for (BooleanData child : _this.children.get(0).children) {
							Not not = new Not();
							not.children.add(child);
							or.children.add(not);
						}
						_this = or;
						_this.hasOptimised = true;
					} else if (_this.children.get(0) instanceof Or) {
						And and = new And();
						for (BooleanData child : _this.children.get(0).children) {
							Not not = new Not();
							not.children.add(child);
							and.children.add(not);
						}
						_this = and;
						_this.hasOptimised = true;
					}
					break;
				}
				case NEGATION -> {
					if (_this.children.get(0) instanceof True) {
						_this = new False();
						_this.hasOptimised = true;
					} else if (_this.children.get(0) instanceof False) {
						_this = new True();
						_this.hasOptimised = true;
					}
					break;
				}
				default -> {
					_this.hasOptimised = false;
				}
			}
			if (!this.equals(_this)) {
//				System.out.println(this.toString() + " --> " + _this.toString() + " (NOT." + law.toString() + ")");
//				QuantrBooleanAlgebra.dump(_this);
				return _this;
			}
		}
		return this;
	}
}
