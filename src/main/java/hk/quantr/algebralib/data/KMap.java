/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib.data;

import hk.quantr.algebralib.MyListener;
import hk.quantr.algebralib.QuantrBooleanAlgebra;
import static hk.quantr.algebralib.QuantrBooleanAlgebra.getTruthTable;
import static hk.quantr.algebralib.QuantrBooleanAlgebra.grayCode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class KMap {

	MyListener listener;
	BooleanData[] inputs;
	int numInputs;
	int numRows;
	int numCols;
	int rowWidth;
	int colWidth;
	public byte[][] board;
	boolean[][] isGrouped;
	public ArrayList<And> sop = new ArrayList<>();
	int[] xDirection = new int[]{0, 1, 0, -1};
	int[] yDirection = new int[]{-1, 0, 1, 0};

	public KMap(String expr) {
		this.listener = QuantrBooleanAlgebra.parse(expr);
		initKMap();
	}

	public KMap(MyListener listener) {
		this.listener = listener;
		initKMap();
	}

	public void initKMap() {
		inputs = QuantrBooleanAlgebra.getInputs(listener);
		numInputs = inputs.length;
		byte[][] truthTable = getTruthTable(listener);
		numRows = 1 << ((numInputs + 1) / 2);
		numCols = 1 << (numInputs / 2);
		board = new byte[numRows][numCols];
		isGrouped = new boolean[numRows][numCols];
		rowWidth = Integer.toBinaryString(numRows - 1).length();
		colWidth = Integer.toBinaryString(numCols - 1).length();
//		for (boolean[] row : isGrouped) {
//			Arrays.fill(row, false);
//		}

		for (int i = 0; i < truthTable.length; i++) {
			int row = 0;
			int col = 0;
			for (int j = 0; j < numInputs; j++) {
				if (j < (numInputs + 1) / 2) {
					row += truthTable[i][j] * (1 << ((numInputs + 1) / 2 - j - 1));
				} else {
					col += truthTable[i][j] * (1 << (numInputs - j - 1));
				}
			}
			board[row][col] = truthTable[i][numInputs];
		}
//		groupKMap();
	}

//	public void groupKMap() {
//		for (int row = 0; row < numRows; row++) {
//			for (int col = 0; col < numCols; col++) {
//				if (!isGrouped[grayCode(row)][grayCode(col)]) {
//					sop.add(findLargestGroup(row, col));
//				}
//			}
//		}
//		System.out.println(sop);
//	}
//
//	public And findLargestGroup(int row, int col) {
//		And and = new And();
//		Set<String> grp = new HashSet<>();
//		isGrouped[grayCode(row)][grayCode(col)] = true;
//		int width = 1;
//		int height = 1;
//		grp.add(zeroPad(grayCode(row), rowWidth) + zeroPad(grayCode(col), colWidth));
//		boolean cont = true;
//		while (cont) {
//			for (int i = 0; i < 4; i++) {
//				cont = true;
//				for (int x = 0; x < width; x++) {
//					for (int y = 0; y < height; y++) {
//						if (board[grayCode(row + x + xDirection[i] * width)][grayCode(col + y + yDirection[i] * height)] == 0) {
//							cont = false;
//						}
//					}
//				}
//				if (cont) {
//					for (int x = 0; x < width; x++) {
//						for (int y = 0; y < height; y++) {
//							isGrouped[grayCode(row + x + xDirection[i] * width)][grayCode(col + y + yDirection[i] * height)] = true;
//							grp.add(zeroPad(row + x + xDirection[i] * width, rowWidth) + zeroPad(grayCode(col + y + yDirection[i] * height), colWidth));
//						}
//					}
//					width += Math.abs(width * xDirection[i]);
//					height += Math.abs(height * yDirection[i]);
//					break;
//				}
//			}
//		}
//		return and;
//	}

	public String zeroPad(int val, int n) {
		return String.format("%" + n + "s", Integer.toBinaryString(grayCode(val))).replace(' ', '0');
	}

	public void print() {

//		System.out.println("K-map of " + listener.output.getExpr() + ":\n");
		System.out.print(" ".repeat(rowWidth) + "|");
		for (int i = 0; i < numCols; i++) {
			System.out.print(zeroPad(i, colWidth) + "|");
		}
		for (int i = (numInputs + 1) / 2; i < numInputs; i++) {
			System.out.print(inputs[i].getName());
		}
		System.out.println();
		for (int i = 0; i < numRows; i++) {
			System.out.println("-".repeat(rowWidth) + ("+" + "-".repeat(colWidth)).repeat(numCols) + "+");
			System.out.print(zeroPad(i, rowWidth));
			for (int j = 0; j < numCols; j++) {
				System.out.print("|" + " ".repeat(colWidth - 1) + board[grayCode(i)][grayCode(j)]);
			}
			System.out.println("|");
		}
		System.out.println("-".repeat(rowWidth) + ("+" + "-".repeat(colWidth)).repeat(numCols) + "'");
		for (int i = 0; i < (numInputs + 1) / 2; i++) {
			System.out.print(inputs[i].getName());
		}
		System.out.println('\n');
	}
}
