package hk.quantr.algebralib.data;

import hk.quantr.algebralib.QuantrBooleanAlgebra;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Output extends BooleanData {

	public Output(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public boolean eval() {
		return children.get(0).eval();
	}

	@Override
	public BooleanData clone() {
		Output output = new Output(this.getName());
		output.children.add(this.children.get(0).clone());
		return output;
	}

	@Override
	public BooleanData optimise(QuantrBooleanAlgebra.Law... laws) {
		Output output = new Output(this.getName());
		output.children.add(children.get(0).optimise(laws));
		output.children.get(0).hasBracket = false;
		if (output.children.get(0).children.size() == 1 && !(output.children.get(0) instanceof Not)) {
			BooleanData temp = output.children.get(0).children.get(0);
			output.children.clear();
			output.children.add(temp);
		} else if ((((output.children.get(0) instanceof Not) || (output.children.get(0) instanceof And) || (output.children.get(0) instanceof Or)) && output.children.get(0).children.isEmpty())) {
			output.children.clear();
			output.children.add(new False());
		}
		return output;
	}
}
