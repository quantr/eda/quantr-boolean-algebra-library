package hk.quantr.algebralib.data;

import hk.quantr.algebralib.QuantrBooleanAlgebra.Law;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public abstract class BooleanData implements Comparator<BooleanData>, Cloneable {

	private String name;
	public ArrayList<BooleanData> children = new ArrayList();
	public BooleanData parent;
	public boolean result = false;
	public boolean hasBracket = false;
	private boolean checkedEqual = false;
	public boolean hasOptimised = false;

	public BooleanData(String name) {
		this.name = name;
	}

	public BooleanData(String name, ArrayList<BooleanData> children) {
		this.name = name;
		this.children = children;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "BooleanData{" + "name=" + name + ", children=" + children + '}';
	}
	
	public String toBddString() {
		return "BooleanData{" + "name=" + name + ", children=" + children + '}';
	}

	public String toMdString() {
		return "BooleanData{" + "name=" + name + ", children=" + children + '}';
	}
	
	public String toIteString(int i, boolean isNot, boolean isFirst) {
		return "BooleanData{" + "name=" + name + ", children=" + children + '}';
	}
	
	public boolean eval() {
		return result;
	}

	public BooleanData optimise(Law... laws) {
		for (BooleanData child : this.children) {
			child = child.optimise(laws);
		}
		return this;
	}

	public boolean equals(BooleanData other) {
		if (!this.name.equals(other.name) || this.children.size() != other.children.size()) {
			return false;
		}

		for (BooleanData otherChild : other.children) {
			otherChild.checkedEqual = false;
		}
		for (BooleanData thisChild : this.children) {
			for (BooleanData otherChild : other.children) {
				if (!otherChild.checkedEqual) {
					if (!thisChild.name.equals(otherChild.name)) {
						// continue;
					} else if (thisChild instanceof Node) {
						otherChild.checkedEqual = true;
					} else if (thisChild.equals(otherChild)) {
						otherChild.checkedEqual = true;
					}
				}
			}
		}
		for (BooleanData child : other.children) {
			if (!child.checkedEqual) {
				return false;
			}
		}
		return true;
	}

	public int compare(BooleanData o1, BooleanData o2) {
		if (o1.equals(o2)) {
			return 0;
		} else {
			return -1;
		}
	}

	public int hasChild(BooleanData b) {
		int pos = -1;
		for (BooleanData child : children) {
			pos++;
			if (child.equals(b)) {
				return pos;
			}
		}
		return -1;
	}

	public String getExpr() {
		return children.stream().map(BooleanData::toString).collect(Collectors.joining(", "));
	}

	
	
	public boolean isOptimisable(Law... laws) {
		laws = Arrays.stream(laws)
				.filter(law -> law != Law.DISTRIBUTIVE)
				.toArray(Law[]::new);
//		System.out.println("\n<optimisability check>");
		BooleanData temp = this.clone().optimise(laws);
//		System.out.println("</" + this.toString() + " --> " + temp.toString() + ": " + !this.equals(temp) + ">\n");
		return !this.equals(temp);
	}

	public boolean isNegationOf(BooleanData b) {
		if (this instanceof Not && this.children.get(0).equals(b)) {
			return true;
		} else if (b instanceof Not && b.children.get(0).equals(this)) {
			return true;
		}
		return false;
	}

	public BooleanData getNegation() {
		if (this instanceof Not) {
			return this.children.get(0);
		} else {
			Not not = new Not();
			not.children.add(this);
			return not;
		}
	}

	@Override
	public BooleanData clone() {
		try {
			return (BooleanData) super.clone();
		} catch (CloneNotSupportedException ex) {
			Logger.getLogger(BooleanData.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}
}
