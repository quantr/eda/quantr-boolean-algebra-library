package hk.quantr.algebralib.data;

import hk.quantr.algebralib.QuantrBooleanAlgebra;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Node extends BooleanData {

	public Node(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public String toBddString() {
		return getName();
	}

	@Override
	public String toMdString() {
		return getName();
	}

	@Override
	public String toIteString(int i, boolean isNot, boolean isFirst) {
		if (isFirst) {
			return getName();
		}
		return "ITE(" + getName() + (isNot ? ",0,1)" : ",1,0)");
	}

	@Override
	public boolean eval() {
		return result;
	}

	@Override
	public BooleanData clone() {
		Node node = new Node(this.getName());
		node.hasBracket = this.hasBracket;
		return node;
	}

	@Override
	public BooleanData optimise(QuantrBooleanAlgebra.Law... laws) {
		return this;
	}
}
