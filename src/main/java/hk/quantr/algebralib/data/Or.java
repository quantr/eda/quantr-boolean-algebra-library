package hk.quantr.algebralib.data;

import hk.quantr.algebralib.QuantrBooleanAlgebra;
import static hk.quantr.algebralib.QuantrBooleanAlgebra.Law.ABSORPTION;
import static hk.quantr.algebralib.QuantrBooleanAlgebra.Law.ASSOCIATIVE;
import static hk.quantr.algebralib.QuantrBooleanAlgebra.Law.DISTRIBUTIVE;
import static hk.quantr.algebralib.QuantrBooleanAlgebra.Law.IDEMPOTENT;
import static hk.quantr.algebralib.QuantrBooleanAlgebra.Law.IDENTITY;
import static hk.quantr.algebralib.QuantrBooleanAlgebra.Law.NEGATION;
import static hk.quantr.algebralib.QuantrBooleanAlgebra.Law.UNIVERSAL_BOUND;
import java.util.stream.Collectors;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Or extends BooleanData {

	public Or() {
		super("OR");
//		this.hasBracket = true;
	}

	@Override
	public String toString() {
		return (hasBracket ? "(" : "") + children.stream()
				.map(BooleanData::toString)
				.collect(Collectors.joining("+")) + (hasBracket ? ")" : "");
	}

	@Override
	public String toBddString() {
		return "or(" + children.stream()
				.map(BooleanData::toBddString)
				.collect(Collectors.joining(",")) + ')';
	}

	@Override
	public String toMdString() {
		return (hasBracket ? "(" : "") + children.stream()
				.map(BooleanData::toMdString)
				.collect(Collectors.joining("+")) + (hasBracket ? ")" : "");
	}
	
	@Override
	public String toIteString(int i, boolean isNot, boolean isFirst) {
		if (i == children.size() - 1) {
			return children.get(i).toIteString(0, isNot, false);
		}
		return "ITE(" + children.get(i).toIteString(0, false, true) + (isNot ? ",0," : ",1,") + this.toIteString(i + 1, isNot, false) + ")";
	}
	
	@Override
	public boolean eval() {
		for (BooleanData b : children) {
			if (b.eval()) {
				result = true;
				return true;
			}
		}
		result = false;
		return false;
	}

	@Override
	public BooleanData clone() {
		Or or = new Or();
		or.hasBracket = this.hasBracket;
		for (BooleanData child : children) {
			or.children.add(child.clone());
		}
		return or;
	}

	@Override
	public BooleanData optimise(QuantrBooleanAlgebra.Law... laws) {
		int _i = -1;
		for (BooleanData child : this.children) {
			this.children.set(++_i, child.optimise(laws));
//			System.out.println(this.children.get(_i).toString() + " [" + this.toString() + "]");
		}
		for (QuantrBooleanAlgebra.Law law : laws) {
			BooleanData _this = this.clone();
			switch (law) {
				case ASSOCIATIVE -> {
					Or or = new Or();
					or.hasBracket = true;
					for (BooleanData child : _this.children) {
						if (child instanceof Or) {
							for (BooleanData c : child.children) {
								or.children.add(c);
								or.hasOptimised = true;
							}
						} else {
							or.children.add(child);
						}
					}
					if (or.hasOptimised) {
						_this = or;
					}
					break;
				}
				case DISTRIBUTIVE -> {
					int pos;
					for (BooleanData child : _this.children) {
						for (BooleanData subChild : child.children) {
							int cnt = 0;
							Or or = new Or();
							or.hasBracket = this.hasBracket;
							And and = new And();
							Or subOr = new Or();
							subOr.hasBracket = true;
							and.children.add(subChild);
							and.children.add(subOr);
							for (BooleanData child2 : _this.children) {
								if ((pos = child2.hasChild(subChild)) > -1) {
									cnt++;
									BooleanData clone = child2.clone();
									clone.children.remove(pos);
									if ((clone instanceof And) || (clone instanceof Or)) {
										if (clone.children.size() == 1) {
											subOr.children.add(clone.children.get(0));
										} else {
											subOr.children.add(clone);
										}
									} else if (!clone.children.isEmpty()) {
										subOr.children.add(clone);
									}
								} else {
									or.children.add(child2);
								}
								if (cnt == 1) {
									cnt++;
									or.children.add(and);
								}
							}
							if (subOr.children.size() > 1 && subOr.isOptimisable(laws)) {
								_this = or;
								_this.hasOptimised = true;
								break;
							}
						}
						if (_this.hasOptimised) {
							break;
						}
					}
					break;
				}
				case IDENTITY -> {
					Or or = new Or();
					or.hasBracket = _this.hasBracket;
					for (int i = 0; i < _this.children.size(); i++) {
						if (_this.children.get(i) instanceof False) {
							or.hasOptimised = true;
						} else {
							or.children.add(_this.children.get(i));
						}
					}
					if (or.hasOptimised) {
						_this = or;
					}
					break;
				}
				case NEGATION -> {
					for (BooleanData child : _this.children) {
						for (BooleanData child2 : _this.children) {
							if (child.isNegationOf(child2)) {
								_this = new True();
								_this.hasOptimised = true;
								break;
							}
						}
					}
					break;
				}
				case IDEMPOTENT -> {
					Or or = new Or();
					or.hasBracket = _this.hasBracket;
					for (BooleanData child : _this.children) {
						if (or.hasChild(child) == -1) {
							or.children.add(child);
						}
					}
					if (!or.equals(_this)) {
						_this = or;
						_this.hasOptimised = true;
					}
					break;
				}
				case UNIVERSAL_BOUND -> {
					for (BooleanData child : _this.children) {
						if (child instanceof True) {
							_this = new True();
							_this.hasOptimised = true;
							break;
						}
					}
					break;
				}
				case ABSORPTION -> {
					Or or = new Or();
					int pos;
					for (BooleanData subChild : _this.children) {
						or.children.add(subChild);
						for (BooleanData child : _this.children) {
//							System.out.println(child.toString() + " / " + subChild.getNegation().toString() + ": " + child.hasChild(subChild.getNegation()));
							if (child instanceof And) {
								if (!child.equals(subChild) && !child.isNegationOf(subChild) && (pos = child.hasChild(subChild.getNegation())) > -1) {
									child.children.remove(pos);
									or.children.add(child);
									or.hasOptimised = true;
								} else if (!child.equals(subChild) && !child.isNegationOf(subChild) && child.hasChild(subChild) == -1) {
									or.children.add(child);
								} else if (!child.equals(subChild) && !child.isNegationOf(subChild) && child.hasChild(subChild) > -1) {
									or.hasOptimised = true;
								}
							} else if (!child.equals(subChild)){
								or.children.add(child);
							}
						}
						if (or.hasOptimised) {
							_this = or;
							_this.hasBracket = true;
							break;
						}
						or.children.clear();
					}
					break;
				}
				default -> {
					_this.hasOptimised = false;
				}
			}
			if (!this.equals(_this)) {
//				System.out.println(this.toString() + " --> " + _this.toString() + " (OR." + law.toString() + ")");
//				QuantrBooleanAlgebra.dump(_this);
				if (_this.children.size() == 1 && !(_this instanceof Not)) {
					return _this.children.get(0);
				}
				return _this;
			}
		}
		return this;
	}
}
