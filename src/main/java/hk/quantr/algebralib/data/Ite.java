/*
 * Copyright 2024 ronald.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib.data;

/**
 *
 * @author ronald
 */
public class Ite {

	private Ite parent = null;
	private Variable variable = null;
	private Terminal terminal = null;
	private Ite condition = null;
	private Ite trueExpression = null;
	private Ite falseExpression = null;

	public Ite(Ite condition, Ite trueExpression, Ite falseExpression) {
		this.condition = condition;
		this.trueExpression = trueExpression;
		this.falseExpression = falseExpression;
	}

	public Ite(Variable variable) {
		this.variable = variable;
	}

	public Ite(Terminal terminal) {
		this.terminal = terminal;
	}

	public boolean isVariable() {
		return this.variable != null && trueExpression == null && falseExpression == null;
	}

	public boolean isTerminal() {
		return this.terminal != null && trueExpression == null && falseExpression == null;
	}

	public boolean isIte() {
		return this.condition != null;
	}

	public Ite getCondition() {
		return condition;
	}

	public Ite getTrueExpression() {
		return trueExpression;
	}

	public Ite getFalseExpression() {
		return falseExpression;
	}

	public Variable getVariable() {
		return variable;
	}

	public Terminal getTerminal() {
		return terminal;
	}

	public void setParent(Ite parent) {
		this.parent = parent;
	}

	public Ite getParent() {
		return parent;
	}

	public Ite getMasterIte() {
		if (!this.condition.isIte()) {
			return this;
		}
		return this.condition.getMasterIte();
	}

	public boolean isTrueExpression() {
		if (parent != null) {
			return this == parent.trueExpression;
		}
		return false;
	}

	public boolean isFalseExpression() {
		if (parent != null) {
			return this == parent.falseExpression;
		}
		return false;
	}

	@Override
	public String toString() {
		if (isVariable()) {
			return this.variable.toString();
		} else if (isTerminal()) {
			return this.terminal.toString();
		}
		return "ITE(" + condition.toString() + ","
				+ trueExpression.toString() + ","
				+ falseExpression.toString() + ')';
	}

	public void update(int value, Ite sub, boolean force) {
		System.out.println("try update: " + condition.toString() + " with " + value + " to " + sub.toString());
		if (this.toString().startsWith("ITE(ITE(ITE")) {
			System.out.println("break");
		}
		if (!sub.isTerminal() && !sub.toString().equals(this.toString())) {
			if (force) {
				boolean skipTrue = false, skipFalse = false;
				System.out.println("force update " + this.toString() + " with " + value + " to " + sub.toString() + ": ");
				if (trueExpression.isTerminal() && trueExpression.terminal.getValue() == value) {
					System.out.print("force updated from " + this.toString() + " to ");
					trueExpression = sub.getMasterIte();
					System.out.println(this.toString());
					skipTrue = true;
				}
				if (falseExpression.isTerminal() && falseExpression.terminal.getValue() == value) {
					System.out.print("force updated from " + this.toString() + " to ");
					falseExpression = sub.getMasterIte();
					System.out.println(this.toString());
					skipFalse = true;
				}
				if (!skipTrue && trueExpression.isIte()) {
					trueExpression.update(value, sub, true);
				}
				if (!skipFalse && falseExpression.isIte()) {
					falseExpression.update(value, sub, true);
				}
				if (condition.isIte()) {
					condition.update(value, sub, true);
				}
				System.out.println("\n" + this.toDot());
			} else if (condition.isIte()) {
				boolean skipTrue = false, skipFalse = false;
				System.out.println("update " + condition.toString() + " with " + value + " to " + sub.toString() + ": ");
				if (condition.trueExpression.isTerminal() && condition.trueExpression.terminal.getValue() == value) {
					System.out.print("updated from " + this.toString() + " to ");
					condition.trueExpression = sub;
					System.out.println(this.toString());
					skipTrue = true;
				}
				/*
ITE(ITE(ITE(a,ITE(b,ITE(c,ITE(e,ITE(g,1,ITE(h,0,1)),ITE(h,0,1)),ITE(d,1,ITE(e,ITE(g,1,ITE(h,0,1)),ITE(h,0,1)))),1),1),1					 ,ITE(ITE(e,ITE(g,1,ITE(h,0,1)),ITE(h,0,1)),1,ITE(h,0,1))),ITE(i,1,ITE(j,0,1)),0)
ITE(ITE(ITE(a,ITE(b,ITE(c,ITE(e,ITE(g,1,ITE(h,0,1)),ITE(h,0,1)),ITE(d,1,ITE(e,ITE(g,1,ITE(h,0,1)),ITE(h,0,1)))),1),1),ITE(i,1,ITE(j,0,1)),ITE(ITE(e,ITE(g,1,ITE(h,0,1)),ITE(h,0,1)),1,ITE(h,0,1))),ITE(i,1,ITE(j,0,1)),0)
				 */
				if (condition.falseExpression.isTerminal() && condition.falseExpression.terminal.getValue() == value) {
					System.out.print("updated from " + this.toString() + " to ");
					condition.falseExpression = sub;
					System.out.println(this.toString());
					skipFalse = true;
				}
				if (!skipTrue && condition.trueExpression.isIte()) {
					condition.trueExpression.update(value, sub, true);
				}
				if (!skipFalse && condition.falseExpression.isIte()) {
					condition.falseExpression.update(value, sub, true);
				}
				System.out.println("\n" + this.toDot());
			}
		}
	}

	public boolean equals(Ite other) {
		if (this.isIte() && other.isIte()) {
			return (this.condition.equals(other.condition)
					&& this.falseExpression.equals(other.falseExpression)
					&& this.trueExpression.equals(other.trueExpression));
		} else if (this.isTerminal() && other.isTerminal()) {
			return this.terminal.getValue() == other.terminal.getValue();
		} else if (this.isVariable() && other.isVariable()) {
			return this.variable.equals(other.variable);
		}
		return false;
	}

	public String getLabel() {
		Ite temp = this.getMasterIte();
		if (temp.isIte()) {
			return temp.condition.toString();
		} else {
			return temp.toString();
		}
	}

	public String toDot() {
		return this.toString() + "[label=" + this.getLabel() + "];\n"
				+ this.toString() + "->" + this.trueExpression.toString() + ";\n"
				+ this.toString() + "->" + this.falseExpression.toString() + "[style=dashed];\n";
	}
}
