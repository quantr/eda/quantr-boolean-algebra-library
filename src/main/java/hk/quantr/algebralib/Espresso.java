/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class Espresso {

	private CubeList set = new CubeList();

	public Espresso(String expr) {
		if (expr.equals("1") || expr.equals("0")) {
		} else {
			CubeList onSet = parseFunction(expr);
			set = irredundant(expand(onSet, complement(onSet)));
		}
	}

	private CubeList parseFunction(String function) {
		CubeList onSet = new CubeList();
		Set<Character> vars = new HashSet<>();
		for (char c : function.toCharArray()) {
			if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
				vars.add(c);
			}
		}
		if (vars.isEmpty()) {
//			onSet.add(new Cube(new int[]{3}));
			return onSet;
		}
		ArrayList<Character> sortedVars = new ArrayList<>(vars);
		Collections.sort(sortedVars, Comparator.comparing(Character::charValue));
		Map<Character, Integer> varMap = new HashMap<>();
		int n = 0;
		for (Character c : sortedVars) {
			varMap.put(c, n++);
		}
		String[] terms = function.split("\\+");
		for (String term : terms) {
			int[] literalValues = new int[sortedVars.size()];
			Arrays.fill(literalValues, 3);
			for (int i = 0; i < term.length(); i++) {
				if (i < term.length() - 1 && term.charAt(i + 1) == '\'') {
					literalValues[varMap.get(term.charAt(i++))] = 2;
				} else {
					literalValues[varMap.get(term.charAt(i))] = 1;
				}
			}
			Cube cube = new Cube(literalValues, sortedVars);
			onSet.put(cube.toString(), cube);
		}
		return onSet;
	}

	public CubeList getSet() {
		return set;
	}

	public static CubeList complement(CubeList cubeList) {
		CubeList complementList = generateFullCubeList(cubeList);
		CubeList split = cubeList.split();
		Iterator<Cube> iter = complementList.values().iterator();
		while (iter.hasNext()) {
			Cube cube = iter.next();
			if (split.contains(cube)) {
				iter.remove();
				iter = complementList.values().iterator();
			}
		}
		return complementList;
	}

	private static CubeList generateFullCubeList(CubeList cubeList) {
		int numVariables = cubeList.entrySet().iterator().next().getValue().size();
		int numCubes = (int) Math.pow(2, numVariables);
		CubeList complementList = new CubeList();
		for (int i = 0; i < numCubes; i++) {
			int[] literalValues = new int[numVariables];
			int value = i;
			for (int j = 0; j < numVariables; j++) {
				int bit = value % 2;
				if (bit == 0) {
					literalValues[j] = 2; // use 2 to represent variable complement
				} else {
					literalValues[j] = 1; // use 1 to represent variable
				}
				value /= 2;
			}
			Cube cube = new Cube(literalValues, cubeList.entrySet().iterator().next().getValue().vars);
			complementList.put(cube.toString(), cube);
		}
		return complementList;
	}

	public static CubeList expand(CubeList onSet, CubeList offSet) {
		onSet.sort(true);
		CubeList result = new CubeList();
		for (Cube cube : onSet.values()) {
			cube.expand(offSet);
			result.put(cube.toString(), cube);
		}
		return result;
	}

	public static CubeList irredundant(CubeList set) {
		Iterator<Cube> iter = set.values().iterator();
		Cube cube;
		while (iter.hasNext()) {
			cube = iter.next();
			if (!set.essential(cube)) {
				iter = set.values().iterator();
			}
		}
		return set;
	}
}
