package hk.quantr.algebralib;

import hk.quantr.algebralib.data.BooleanData;
import hk.quantr.algebralib.data.Node;
import hk.quantr.algebralib.antlr.BooleanAlgebraLexer;
import hk.quantr.algebralib.antlr.BooleanAlgebraParser;
import hk.quantr.algebralib.antlr.IteLexer;
import hk.quantr.algebralib.antlr.IteParser;
import hk.quantr.algebralib.data.KMap;
import hk.quantr.algebralib.data.Output;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrBooleanAlgebra {

	private static void dump(BooleanData n, int level) {
		System.out.println("-".repeat(level) + " " + n.getName() + " (" + n.children.size() + ")");
		for (BooleanData d : n.children) {
			dump(d, level + 2);
		}
	}

	public static void dump(BooleanData n) {
		dump(n, 0);
	}

	public static MyListener parse(String expr) {
		BooleanAlgebraLexer lexer = new BooleanAlgebraLexer(CharStreams.fromString(expr));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		BooleanAlgebraParser parser = new BooleanAlgebraParser(tokenStream);
		ParseTree tree = parser.parse();
		ParseTreeWalker walker = new ParseTreeWalker();
		MyListener l = new MyListener();
		walker.walk(l, tree);
		return l;
	}

	public static Node[] getInputs(MyListener listener) {
		return listener.getInputs().toArray(Node[]::new);
	}

	public static BooleanData getOutput(MyListener listener) {
		return listener.getOutput();
	}

	public static void printTruthTable(MyListener listener) {
		System.out.println("." + "-".repeat(getInputs(listener).length * 2 + 1) + ".");
		System.out.print("|" + Arrays.stream(getInputs(listener)).map(BooleanData::toString).collect(Collectors.joining("|")));
		System.out.println("|" + getOutput(listener) + "|");
		System.out.println("|" + "-+".repeat(getInputs(listener).length) + "-|");
		System.out.println(Arrays.stream(getTruthTable(listener))
				.map(bytes -> {
					StringBuilder sb = new StringBuilder();
					sb.append("|");
					for (byte b : bytes) {
						sb.append(b);
						sb.append("|");
					}
//					sb.deleteCharAt(sb.length() - 1);
					return sb.toString();
				})
				.collect(Collectors.joining("\n")));
		System.out.println("'" + "-".repeat(getInputs(listener).length * 2 + 1) + "'");
	}

	public static String getTruthTableAsString(MyListener listener) {
		StringBuilder sb = new StringBuilder();
		sb.append(".").append("-".repeat(getInputs(listener).length * 2 + 1)).append(".").append("\n");
		sb.append("|").append(Arrays.stream(getInputs(listener)).map(BooleanData::toString).collect(Collectors.joining("|"))).append("|").append(getOutput(listener)).append("|").append("\n");
		sb.append("|").append("-+".repeat(getInputs(listener).length)).append("-|").append("\n");
		sb.append(Arrays.stream(getTruthTable(listener))
				.map(bytes -> {
					StringBuilder row = new StringBuilder("|");
					for (byte b : bytes) {
						row.append(b).append("|");
					}
					row.replace(row.length() - 2, row.length() - 1, row.charAt(row.length() - 2) == '1' ? "T" : "F");
					return row.toString();
				})
				.collect(Collectors.joining("\n"))
		).append("\n");
		sb.append("'").append("-".repeat(getInputs(listener).length * 2 + 1)).append("'").append("\n");
		return sb.toString();
	}

	public static byte[][] getTruthTable(MyListener listener) {
		BooleanData[] inputs = getInputs(listener);
		int numInputs = inputs.length;
		int numCombinations = (int) Math.pow(2, numInputs);
		byte[][] table = new byte[numCombinations][numInputs + 1];

		for (int i = 0; i < numCombinations; i++) {
			int temp = i;
			for (int j = 0; j < numInputs; j++) {
				inputs[j].result = (temp >= (int) Math.pow(2, numInputs - j - 1));
				table[i][j] = (byte) (inputs[j].result ? 1 : 0);
				if (inputs[j].result) {
					temp -= Math.pow(2, numInputs - j - 1);
				}
			}
			table[i][numInputs] = (byte) (getOutput(listener).eval() ? 1 : 0);
		}

		return table;
	}

	public static String[] getSOP(MyListener listener) {
		String[] pool = new String[]{"0", "1", "[01]"};
		int numInputs = listener.getInputs().size();
		int numCombinations = (int) Math.pow(pool.length, numInputs);
		String[] patterns = new String[numCombinations - 1];
		Set<String> pi = new HashSet();
		Map<Integer, String> minterms = new HashMap<>();
		Set<String> sop = new HashSet();

		for (int i = 0; i < numCombinations - 1; i++) {
			StringBuilder sb = new StringBuilder();
			int temp = i;
			for (int j = 0; j < numInputs; j++) {
				int index = temp % pool.length;
				sb.insert(0, pool[index]);
				temp /= pool.length;
			}
			patterns[i] = sb.toString();
		}
		Arrays.sort(patterns, Comparator.comparingInt(s -> ((String) s).replaceAll("\\[01\\]", "").length()).reversed());
		int temp = 0;
		for (byte[] row : getTruthTable(listener)) {
			if (row[numInputs] == 1) {
				minterms.put(temp, Arrays.toString(row).replaceAll("[\\[\\]\\s,]", "").substring(0, numInputs));
			}
			temp++;
		}
		listener.progress = 5;
		for (String regex : patterns) {
			int cnt = 0;
			for (byte[] row : getTruthTable(listener)) {
				if (Arrays.toString(row).replaceAll("[\\[\\]\\s,]", "").substring(0, numInputs).matches(regex) && row[numInputs] == 1) {
					cnt++;
				}
			}
			if (cnt == Math.pow(2, regex.replaceAll("[01]", "").length() / 2)) {
				pi.removeIf(str -> str.matches(regex.replaceAll("\\[01\\]", "(1|0|\\\\[01\\\\])")));
				boolean flag = true;
				for (String str : pi) {
					if (regex.matches(str.replaceAll("\\[01\\]", "(1|0|\\\\[01\\\\])"))) {
						flag = false;
					}
				}
				if (flag) {
					pi.add(regex);
				}
			}
		}
		listener.progress = 40;
		Iterator<Integer> iter = minterms.keySet().iterator();
		while (iter.hasNext()) {
			int i = iter.next();
			int cnt = 0;
			String epi = "";
			for (String regex : pi) {
				if (minterms.get(i).matches(regex.replaceAll("\\[01\\]", "(1|0|\\\\[01\\\\])"))) {
					cnt++;
					epi = regex;
				}
			}
			final String _epi = epi;
			if (cnt == 1) {
				sop.add(epi);
				minterms.values().removeIf(mt -> mt.matches(_epi.replaceAll("\\[01\\]", "(1|0|\\\\[01\\\\])")));
				pi.remove(epi);
				iter = minterms.keySet().iterator();
			}
		}
		listener.progress = 60;
		Iterator<String> iter2 = pi.iterator();
		while (iter2.hasNext()) {
			String i = iter2.next();
			boolean flag = true;
			for (String term : minterms.values()) {
				if (term.matches(i.replaceAll("\\[01\\]", "(1|0|\\\\[01\\\\])"))) {
					flag = false;
					break;
				}
			}
			if (flag) {
				pi.remove(i);
				iter2 = pi.iterator();
			}
		}
		listener.progress = 80;
		Iterator<String> iter3 = pi.iterator();
		while (iter3.hasNext()) {
			String i = iter3.next();
			int cnt = 0;
			int max = 0;
			String epi = "";
			for (String term : minterms.values()) {
				if (term.matches(i.replaceAll("\\[01\\]", "(1|0|\\\\[01\\\\])"))) {
					if (max != (max = Math.max(++cnt, max))) {
						epi = i;
					}
				}
			}
			final String _epi = epi;
			if (max > 0) {
				sop.add(epi);
				minterms.values().removeIf(mt -> mt.matches(_epi.replaceAll("\\[01\\]", "(1|0|\\\\[01\\\\])")));
				pi.remove(epi);
				iter3 = pi.iterator();
			}
		}
		listener.progress = 100;
		if (sop.isEmpty()) {
			return new String[]{"0"};
		}
		String[] res = new String[sop.size()];
		int n = 0;
		for (String str : sop) {
			String product = "";
			for (int i = 0; i < numInputs; i++) {
				if (str.replaceAll("\\[01\\]", "x").charAt(i) == '1') {
					product += getInputs(listener)[i];
				} else if (str.replaceAll("\\[01\\]", "x").charAt(i) == '0') {
					product += getInputs(listener)[i] + "'";
				}
			}
			res[n++] = product;
		}
		return res;
	}

	public static MyListener eOptimise(MyListener listener) {
		listener.output = (Output) QuantrBooleanAlgebra.baOptimise(listener.output);
		String expr = QuantrBooleanAlgebra.baOptimise(listener.output).getExpr();
		System.out.println(expr);
		if (expr.equals("0") || expr.equals("1")) {
			return listener;
		}
		Espresso espresso = new Espresso(expr);
//		System.out.println(espresso.getSet());
		MyListener listener2 = parse(listener.output.toString() + "=" + espresso.getSet());
		for (Node node : getInputs(listener)) {
			boolean found = false;
			for (Node node2 : getInputs(listener2)) {
				if (node2.getName().equals(node.getName())) {
					found = true;
					break;
				}
			}
			if (!found) {
				listener2.inputs.add(new Node(node.getName()));
			}
		}
		listener2.inputs = listener2.sorted(listener2.inputs);
		System.out.println(listener.output.getExpr().length() > listener2.output.getExpr().length() ? "oooooooooo optimised with EPRESSO oooooooooo" : "xxxxxxxxxx optimised with BOOLEAN ALGEBRA xxxxxxxxxx");
		System.out.println("Boolean Algebra:\n" + listener.output + "=" + expr);
		return listener2;
	}

	public static BooleanData qmOptimise(BooleanData b) {
		MyListener listener = parse(b + "=" + b.getExpr());
		MyListener listener2 = parse(b + "=" + String.join("+", getSOP(listener)));
		for (Node node : getInputs(listener)) {
			boolean found = false;
			for (Node node2 : getInputs(listener2)) {
				if (node2.getName().equals(node.getName())) {
					found = true;
					break;
				}
			}
			if (!found) {
				listener2.inputs.add(new Node(node.getName()));
			}
		}
		listener2.inputs = listener2.sorted(listener2.inputs);
		return listener2.output;
	}

	public static MyListener qmOptimise(MyListener listener) {
		MyListener listener2 = parse(listener.output + "=" + String.join("+", getSOP(listener)));
		for (Node node : getInputs(listener)) {
			boolean found = false;
			for (Node node2 : getInputs(listener2)) {
				if (node2.getName().equals(node.getName())) {
					found = true;
					break;
				}
			}
			if (!found) {
				listener2.inputs.add(new Node(node.getName()));
			}
		}
		listener2.inputs = listener2.sorted(listener2.inputs);
		return listener2;
	}

	/*
	U -> Useless
	D -> Done
	B -> Buggy
	 */
	public enum Law {
		UNIVERSAL_BOUND, // A0 = 0			D	A+1 = 1
		NEGATION, // A!A = 0			D	A+!A = 1
		IDENTITY, // A1 = A			D	A+0 = A
		ASSOCIATIVE, // (AB)C = ABC		D	(A+B)+C = A+B+C
		IDEMPOTENT, // AA = A			D	A+A = A
		DOUBLE_NEGATIVE, // !!A = A			D	
		ABSORPTION, // A(A+B) = A		D	A+AB = A
		DE_MORGANS, // !(AB) = !A+!B	D	!(A+B) = !A!B
		DISTRIBUTIVE, // A(B+C) = AB+AC	D	AB+AC = A(B+C)
		COMMUNTATIVE,		// AB = BA			U	A+B = B+A			
	}

	public static MyListener baOptimise(MyListener listener) {
		MyListener listener2 = parse(listener.output + "=" + listener.output.getExpr());
		listener2.output = (Output) baOptimise(listener2.output, Law.ABSORPTION, Law.ASSOCIATIVE, Law.COMMUNTATIVE, Law.DE_MORGANS, Law.DISTRIBUTIVE,
				Law.DOUBLE_NEGATIVE, Law.IDEMPOTENT, Law.IDENTITY, Law.NEGATION, Law.UNIVERSAL_BOUND).clone();
		for (Node node : getInputs(listener)) {
			boolean found = false;
			for (Node node2 : getInputs(listener2)) {
				if (node2.getName().equals(node.getName())) {
					found = true;
					break;
				}
			}
			if (!found) {
				listener2.inputs.add(new Node(node.getName()));
			}
		}
		listener2.inputs = listener2.sorted(listener2.inputs);
		return listener2;
	}

	public static BooleanData baOptimise(BooleanData b) {
		b = baOptimise(b, Law.ABSORPTION, Law.ASSOCIATIVE, Law.COMMUNTATIVE, Law.DE_MORGANS, Law.DISTRIBUTIVE,
				Law.DOUBLE_NEGATIVE, Law.IDEMPOTENT, Law.IDENTITY, Law.NEGATION, Law.UNIVERSAL_BOUND).clone();
		return b;
	}

	public static BooleanData baOptimise(BooleanData b, Law... laws) {
		Arrays.sort(laws, Comparator.comparingInt(law -> law.ordinal()));
		BooleanData temp;
		while (true) {
			temp = b.clone().optimise(laws);
			if (temp.equals(b)) {
				break;
			}
			b = temp;
		}
		return temp;
	}

	public static int grayCode(int n) {
		return n ^ (n >> 1);
	}

	public static void printKMap(MyListener listener) {
		KMap k = new KMap(listener);
		k.print();
	}
	
//	public static IteListener parseIte(String expr) {
//		IteLexer lexer = new IteLexer(CharStreams.fromString(expr));
//		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//		IteParser parser = new IteParser(tokenStream);
//		ParseTree tree = parser.parse();
//		ParseTreeWalker walker = new ParseTreeWalker();
//		IteListener l = new IteListener();
//		walker.walk(l, tree);
//		return l;
//	}
	
	public static IteListener parseIte(String expr, String order) {
		IteLexer lexer = new IteLexer(CharStreams.fromString(expr));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		IteParser parser = new IteParser(tokenStream);
		ParseTree tree = parser.parse();
		ParseTreeWalker walker = new ParseTreeWalker();
		IteListener l = new IteListener();
		walker.walk(l, tree);
		return l;
	}
	
//	public static void main(String[] args) {
//		String str = parse("o=abc").output.children.get(0).toIteString(0, false, false);
//		System.out.println("parse: " + str);
//		IteListener ite = parseIte(str);
////		ite.buildDotGraph();
//		return;
//	}
}
