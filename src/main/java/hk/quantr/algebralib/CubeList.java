/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class CubeList extends LinkedHashMap<String, Cube> {

	public CubeList() {
		super();
	}

	public boolean add(Cube cube) {
		if (this.containsKey(cube.toString())) {
			return false;
		}
		this.put(cube.toString(), cube);
		return true;
	}

	public boolean addAll(Collection<? extends Cube> c) {
		boolean modified = false;
		for (Cube cube : c) {
			if (!this.contains(cube) && this.add(cube)) {
				modified = true;
			}
		}
		return modified;
	}

	public boolean contains(Object o) {
		for (Cube c : this.values()) {
			if (((Cube) o).equals(c)) {
				return true;
			}
		}
		return false;
	}

	public CubeList copy() {
		CubeList cubeList = new CubeList();
		for (Cube cube : this.values()) {
			Cube temp = cube.copy();
			cubeList.add(cube.copy());
		}
		return cubeList;
	}

	@Override
	public String toString() {
		String res = this.values().stream().map(Cube::toString).collect(Collectors.joining("+"));
		return res.length() == 0 ? "1" : res;
	}

	public CubeList split() {
		CubeList cubeList = new CubeList();
		for (Cube cube : this.values()) {
			for (Cube minterm : cube.split().values()) {
				cubeList.add(minterm);
			}
		}
		return cubeList;
	}

	public int numVars() {
		return this.entrySet().iterator().next().getValue().size();
	}

	public int[] getOnes() {
		int numVars = this.numVars();
		int[] columnSum = new int[numVars * 2];
		for (Cube cube : this.values()) {
			for (int i = 0; i < numVars; i++) {
				int literal = cube.getLiteralValueAt(i);
				columnSum[i * 2] += literal >> 1;
				columnSum[i * 2 + 1] += literal & 1;
			}
		}
		return columnSum;
	}

	public void sort(boolean ascending) {
		int[] bitSums = getOnes();
		List<Map.Entry<String, Cube>> entryList = new ArrayList<>(this.entrySet());
		if (ascending) {
			entryList.sort(Comparator.comparing(entry -> entry.getValue().getWeight(bitSums)));
		} else {
			entryList.sort(Comparator.comparing(entry -> entry.getValue().getWeight(bitSums), Comparator.reverseOrder()));
		}
		this.clear();
		entryList.forEach(entry -> this.put(entry.getKey(), entry.getValue()));
	}

	public boolean intersects(Cube otherCube) {
		for (Cube cube : this.values()) {
			if (cube.intersects(otherCube)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean essential(Cube other){
		for (Cube cube : this.values()) {
			if (cube != other && cube.covers(other)) {
				this.remove(other.toString());
				return false;
			}
			if (cube != other && this.remove(other.add(cube).toString()) != null) {
				return false;
			}
		}
		return true;
	}
}
