/*
 * Copyright 2024 ronald.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import hk.quantr.algebralib.antlr.IteParser;
import hk.quantr.algebralib.antlr.IteParserBaseListener;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ronald
 */
public class IteListenerOld extends IteParserBaseListener {

	private StringBuilder dotGraph = new StringBuilder();
	private Set<String> nodes = new HashSet();
	private boolean trueDone = false, falseDone = false;

	@Override
	public void exitIte(IteParser.IteContext ctx) {
		if (((ctx.parent.getChildCount() >= 6 && ctx.parent.getChild(2).getText().startsWith("ITE")) || ctx.getText().startsWith("ITE"))) {
			if ("1".equals(ctx.getText())) {
				System.out.println("(break)");
			}
			System.out.println("test: " + ctx.getText());
			if (trueDone && falseDone) {
				falseDone = false;
				trueDone = false;
				replaceAll(dotGraph, "->*", "->", 0, dotGraph.length());
			}
			if (ctx.parent.getChildCount() > 2) {
				if (ctx.getText().equals(ctx.parent.getChild(4).getText())) {
					System.out.println("#?????" + ctx.parent.getChild(2).getText() + dotGraph.indexOf("#+" + ctx.parent.getChild(2).getText()));
					replaceAll(dotGraph, "->1", "->*" + getMasterIf(ctx).getText(), dotGraph.indexOf("#+" + ctx.parent.getChild(2).getText()), dotGraph.indexOf("#-" + ctx.parent.getChild(2).getText()));
					trueDone = true;
					System.out.println("\n" + dotGraph.toString());
				} else if (ctx.getText().equals(ctx.parent.getChild(6).getText())) {
					System.out.println("#!!!!!" + ctx.parent.getChild(2).getText() + dotGraph.indexOf("#+" + ctx.parent.getChild(2).getText()));
					replaceAll(dotGraph, "->0", "->*" + getMasterIf(ctx).getText(), dotGraph.indexOf("#+" + ctx.parent.getChild(2).getText()), dotGraph.indexOf("#-" + ctx.parent.getChild(2).getText()));
					falseDone = true;
					System.out.println("\n" + dotGraph.toString());
				}
			}
			if (ctx.getText().startsWith("ITE")) {
				if (addNode(getMasterIf(ctx))) {
					dotGraph.append(ctx.getText()).append("->").append(ctx.ite().get(1).getText()).append(";\n");
					dotGraph.append(ctx.getText()).append("->").append(ctx.ite().get(2).getText()).append("[style=dashed];\n");
				}
				dotGraph.append("#-").append(ctx.getText()).append("\n");
				System.out.println("\n" + dotGraph.toString());
			}
		}
	}

	@Override
	public void exitOBracket(IteParser.OBracketContext ctx) {
		dotGraph.append("#+").append(ctx.parent.getText()).append("\n");
	}

	public boolean addNode(IteParser.IteContext ctx) {
		if (!nodes.contains(ctx.getText()) && ctx.getText().contains(",") && !ctx.getChild(2).getText().startsWith("ITE")) {
			System.out.println("add: " + ctx.getText());
			dotGraph.append(ctx.getText()).append("[label=").append(ctx.getText().charAt(ctx.getText().indexOf(",") - 1)).append("];\n");
			nodes.add(ctx.getText());
			return true;
		}
		return false;
	}

	public String getDotGraph() {
		return dotGraph.toString();
	}

	public static void replaceAll(StringBuilder builder, String from, String to, int rangeStart, int rangeEnd) {
		int index = builder.indexOf(from, rangeStart);
		System.out.println("from: \n" + from);
		while (index != -1 && index < rangeEnd) {
			builder.replace(index, index + from.length(), to);
			index += to.length(); // Move to the end of the replacement
			index = builder.indexOf(from, index);
		}
		System.out.println("\nto: \n" + to);
	}

	public static IteParser.IteContext getMasterIf(IteParser.IteContext ctx) {
		try {
			if (!ctx.getChild(2).getText().startsWith("ITE")) {
				return ctx;
			}
			return getMasterIf((IteParser.IteContext) ctx.getChild(2));
		} catch (Exception e) {
			return ctx;
		}
	}
}
