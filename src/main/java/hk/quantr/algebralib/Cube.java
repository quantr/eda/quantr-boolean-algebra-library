/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import java.util.ArrayList;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class Cube {

	private int[] literalValues; // 00=0=void, 01=1=var, 10=2=var', 11=3=dc
	public ArrayList<Character> vars = new ArrayList();

	public Cube(int[] literalValues, ArrayList<Character> vars) {
		this.literalValues = literalValues;
		this.vars = vars;
	}

	public Cube(int[] literalValues) {
		this.literalValues = literalValues;
	}

	public int size() {
		return this.literalValues.length;
	}

	public int getLiteralValueAt(int i) {
		return this.literalValues[i];
	}

	public int[] getLiteralValues() {
		return literalValues;
	}

	public void setLiteralValue(int index, int value) {
		literalValues[index] = value;
	}

	public boolean covers(Cube other) {
		for (int i = 0; i < other.size(); i++) {
			if (literalValues[i] != 3 && literalValues[i] != other.getLiteralValueAt(i)) {
				return false;
			}
		}
		return true;
	}

	public boolean intersects(Cube other) {
		for (int i = 0; i < literalValues.length; i++) {
			if ((literalValues[i] & other.literalValues[i]) == 0) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < literalValues.length; i++) {
			if (literalValues[i] == 2) {
				sb.append(vars.get(i));
				sb.append('\'');
			} else if (literalValues[i] == 1) {
				sb.append(vars.get(i));
			}
		}
		return sb.toString();
	}

	public CubeList split() {
		CubeList cubes = new CubeList();
		if (!containsDontCare()) {
			cubes.add(this);
			return cubes;
		}
		int dcIndex = -1;
		for (int i = 0; i < literalValues.length; i++) {
			if (literalValues[i] == 3) {
				dcIndex = i;
				break;
			}
		}
		int[] literalValues1 = literalValues.clone();
		int[] literalValues2 = literalValues.clone();
		literalValues1[dcIndex] = 1;
		literalValues2[dcIndex] = 2;
		Cube cube1 = new Cube(literalValues1, this.vars);
		cubes.addAll(cube1.split().values());
		Cube cube2 = new Cube(literalValues2, this.vars);
		cubes.addAll(cube2.split().values());
		return cubes;
	}

	private boolean containsDontCare() {
		for (int literalValue : literalValues) {
			if (literalValue == 3) {
				return true;
			}
		}
		return false;
	}

	public boolean equals(Cube other) {
		for (int i = 0; i < this.size(); i++) {
			if (this.literalValues[i] != other.literalValues[i]) {
				return false;
			}
		}
		return true;
	}

	public Cube copy() {
		return new Cube(this.literalValues.clone(), this.vars);
	}

	public int getWeight(int[] bitSums) {
		int sum = 0;
		for (int i = 0; i < this.size(); i++) {
			sum += (this.literalValues[i] >> 1) * bitSums[i * 2] + (this.literalValues[i] & 1) * bitSums[i * 2 + 1];
		}
		return sum;
	}

	public void expand(CubeList offSet) {
		for (int i = 0; i < this.size(); i++) {
			this.raise(i, offSet);
		}
	}

	public boolean raise(int index, CubeList offSet) {
		int temp = this.literalValues[index];
		this.literalValues[index] = 3;
		if (offSet.intersects(this)) {
			this.literalValues[index] = temp;
			return false;
		}
		return true;
	}

	public Cube add(Cube other) {
		Cube cube = this.copy();
		for (int i = 0; i < cube.size(); i++) {
			if (this.getLiteralValueAt(i) + other.getLiteralValueAt(i) == 3) {
				cube.setLiteralValue(i, 3);
			} else {
				cube.setLiteralValue(i, Math.min(this.getLiteralValueAt(i), other.getLiteralValueAt(i)));
			}
		}
		return cube;
	}
}
