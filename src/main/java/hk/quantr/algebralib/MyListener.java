package hk.quantr.algebralib;

import hk.quantr.algebralib.data.And;
import hk.quantr.algebralib.data.BooleanData;
import hk.quantr.algebralib.data.False;
import hk.quantr.algebralib.data.Node;
import hk.quantr.algebralib.data.Output;
import hk.quantr.algebralib.data.Not;
import hk.quantr.algebralib.data.Or;
import hk.quantr.algebralib.data.True;
import hk.quantr.algebralib.antlr.BooleanAlgebraParser;
import hk.quantr.algebralib.antlr.BooleanAlgebraParserBaseListener;
import hk.quantr.algebralib.antlr.IteLexer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyListener extends BooleanAlgebraParserBaseListener {

	public Output output;
	private int level = -1;
	public LinkedHashMap<Integer, ArrayList<BooleanData>> buffer = new LinkedHashMap<>();
	private LinkedHashMap<Integer, Integer> stamp = new LinkedHashMap<>();
	public Set<Node> inputs = new HashSet();
	public int progress = 0;

	@Override
	public void exitTerm(BooleanAlgebraParser.TermContext ctx) {
		BooleanData b = null;
		if (buffer.get(level).size() > (int) stamp.get(level) + 1) {
			And and = new And();
			for (int i = buffer.get(level).size() - stamp.get(level); i > 0; i--) {
				and.children.add(buffer.get(level).remove((int) stamp.get(level)));
			}
			b = and;
		}
		if (b != null) {
			buffer.get(level).add(b);
		}
		if (!ctx.plus().isEmpty()) {
			Or or = new Or();
			while (!buffer.get(level).isEmpty()) {
				or.children.add(buffer.get(level).remove(0));
			}
			b = or;
		} else {
			b = buffer.get(level).get(0);
		}
		buffer.get(level - 1).add(b);

		buffer.remove(level);
		stamp.remove(level--);

	}

	@Override
	public void exitCBracket(BooleanAlgebraParser.CBracketContext ctx) {
		buffer.get(level).get(buffer.get(level).size() - 1).hasBracket = true;
	}

	@Override
	public void enterTerm(BooleanAlgebraParser.TermContext ctx) {
		buffer.put(++level, new ArrayList<>());
		stamp.put(level, 0);
	}

	@Override
	public void exitPlus(BooleanAlgebraParser.PlusContext ctx) {
		if (buffer.get(level).size() > (int) stamp.get(level) + 1) {
			And and = new And();
			while (buffer.get(level).size() > stamp.get(level)) {
				and.children.add(buffer.get(level).remove((int) stamp.get(level)));
			}
			buffer.get(level).add(and);
		}
		stamp.put(level, stamp.get(level) + 1);
	}

	@Override
	public void exitFactor(BooleanAlgebraParser.FactorContext ctx) {
		if (ctx.NOT() != null || (ctx.var() != null && ctx.var().NOT() != null)) {
			int numNots = ctx.var() != null ? ctx.var().getChildCount() - 1 : ctx.getChildCount() - 3;
			for (int i = 0; i < numNots; i++) {
				Not not = new Not();
				not.children.add(buffer.get(level).remove(buffer.get(level).size() - 1));
				buffer.get(level).add(not);
			}
		}
	}

	@Override
	public void exitVar(BooleanAlgebraParser.VarContext ctx) {
		if (ctx.VARIABLE().getText().equals("1")) {
			buffer.get(level).add(new True());
		} else if (ctx.VARIABLE().getText().equals("0")) {
			buffer.get(level).add(new False());
		} else {
			buffer.get(level).add(newNode(ctx.VARIABLE().getText()));
		}
	}

	@Override
	public void enterOutput(BooleanAlgebraParser.OutputContext ctx) {
		output = new Output(ctx.VARIABLE().getText());
		buffer.put(-1, new ArrayList<>());
	}

	@Override
	public void exitParse(BooleanAlgebraParser.ParseContext ctx) {
		while (!buffer.get(-1).isEmpty()) {
			output.children.add(buffer.get(-1).remove(0));
		}
		inputs = sorted(inputs);
	}

	private Node newNode(String name) {
		for (Node node : inputs) {
			if (node.getName().equals(name)) {
				return node;
			}
		}
		Node node = new Node(name);
		inputs.add(node);
		return node;
	}

	public Set<Node> sorted(Set<Node> inputSet) {
		List<Node> sortedList = new ArrayList<>(inputSet);
		Collections.sort(sortedList, Comparator.comparing(BooleanData::getName));
		return new LinkedHashSet<>(sortedList);
	}

	public BooleanData getOutput() {
		return output;
	}

	public Set<Node> getInputs() {
		return inputs;
	}
}
