parser grammar IteParser;

options {
  tokenVocab = IteLexer;
}

@header {
}

@parser::members{
}

parse	: ite EOF;

ite		: ITE oBracket ite COMMA ite COMMA ite cBracket
		| VARIABLE
		| TERMINAL;

oBracket: OPEN_BRACKET;

cBracket: CLOSE_BRACKET;