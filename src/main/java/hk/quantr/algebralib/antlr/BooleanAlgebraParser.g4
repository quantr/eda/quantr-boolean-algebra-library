parser grammar BooleanAlgebraParser;

options {
  tokenVocab = BooleanAlgebraLexer;
}

@header {
}

@parser::members{
}

parse	: output EQUAL term EOF;

output	: VARIABLE;

term	: factor (plus? factor)*;

plus	: PLUS;

oBracket: OPEN_BRACKET;

cBracket: CLOSE_BRACKET;

factor	: var
		| oBracket term cBracket NOT*
		;

var		: VARIABLE NOT*;