lexer grammar BooleanAlgebraLexer;

PLUS            : '+';
EQUAL			: '=';

OPEN_BRACKET	: '(';
CLOSE_BRACKET	: ')';

NOT				: '\'';
VARIABLE		: [a-zA-Z01];

WS				: [ \t]+ -> skip;
NL				: '\r'? '\n' -> skip;