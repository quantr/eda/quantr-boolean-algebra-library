lexer grammar IteLexer;

ITE				: 'ITE';
OPEN_BRACKET	: '(';
CLOSE_BRACKET	: ')';
COMMA			: ',';
VARIABLE		: [a-zA-Z];
TERMINAL		: [01];

WS				: [ \t]+ -> skip;
NL				: '\r'? '\n' -> skip;