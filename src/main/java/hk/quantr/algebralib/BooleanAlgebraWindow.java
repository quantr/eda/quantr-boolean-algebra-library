/*
 * Copyright 2024 ronald.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JScrollPane;

/**
 *
 * @author ronald
 */
public class BooleanAlgebraWindow extends JFrame {

	private String input;
	private String output;

	private JTextField inputField;
	private JLabel outputLabel;
	private JLabel truthTableLabel;
	private GraphPanel graphPanel;
//	private GraphPanel graphPanel1;
	private JLabel mdLabel;
	private JLabel iteLabel;

	public BooleanAlgebraWindow() {
		JFrame frame = new JFrame("Boolean Algebra Window");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1000, 800);
		frame.setLayout(new BorderLayout());

		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets(5, 5, 5, 5);

		JLabel inputLabel = new JLabel("Input: ");
		inputField = new JTextField(20);

		graphPanel = new GraphPanel();
//		graphPanel1 = new GraphPanel();

		outputLabel = new JLabel();
		JButton outputCopyButton = new JButton("Copy BDD Code");
		outputCopyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				copyToClipboard(outputLabel.getText());
			}
		});

		mdLabel = new JLabel();
		JButton mdCopyButton = new JButton("Copy MarkDown");
		mdCopyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				copyToClipboard(mdLabel.getText());
			}
		});

		iteLabel = new JLabel();
		JButton iteCopyButton = new JButton("Copy ITE");
		iteCopyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				copyToClipboard(iteLabel.getText());
			}
		});

		truthTableLabel = new JLabel();

		constraints.gridx = 0;
		constraints.gridy = 0;
		panel.add(inputLabel, constraints);

		constraints.gridx = 1;
		panel.add(inputField, constraints);

		constraints.gridx = 0;
		constraints.gridwidth = 2;

		constraints.gridy = 1;
		panel.add(outputLabel, constraints);

		constraints.gridy = 2;
		panel.add(outputCopyButton, constraints);

		constraints.gridy = 3;
		panel.add(mdLabel, constraints);

		constraints.gridy = 4;
		panel.add(mdCopyButton, constraints);

		constraints.gridy = 5;
		panel.add(iteLabel, constraints);

		constraints.gridy = 6;
		panel.add(iteCopyButton, constraints);

		constraints.gridy = 7;
		panel.add(truthTableLabel, constraints);

		constraints.insets = new Insets(5, 100, 5, 5); // Reset insets for the empty panel

		constraints.gridx = 1;
		panel.add(graphPanel, constraints);
//		constraints.gridx = 2;
//		panel.add(graphPanel1, constraints);

		JScrollPane scrollPane = new JScrollPane(panel);
		frame.add(scrollPane, BorderLayout.CENTER);

		inputField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				input = inputField.getText();
				computeOutput();
			}
		});

		frame.setVisible(true);
	}

	public static void main(String[] args) {
		BooleanAlgebraWindow window = new BooleanAlgebraWindow();
	}

	private void computeOutput() {
		MyListener listener = QuantrBooleanAlgebra.parse("o=" + input); // bcd'+ad'+ad'+a'd'
		iteLabel.setText(listener.output.children.get(0).toIteString(0, false, false));
		output = listener.output.children.get(0).toBddString();
		outputLabel.setText(("order(" + new StringBuilder(listener.getInputs().toString().substring(1, listener.getInputs().toString().length() - 1)).reverse().toString()
				+ ")\n" + output + "\nprint($1)").replaceAll("f", "z").replaceAll("t", "y").replaceAll("noy", "not").replace("priny", "print").replaceAll(" ", ""));
		copyToClipboard(outputLabel.getText());
		mdLabel.setText(listener.output.children.get(0).toMdString());
		String truthTable = QuantrBooleanAlgebra.getTruthTableAsString(listener);
		System.out.println(truthTable);
		truthTableLabel.setText("<html><pre>" + truthTable + "</pre></html>");
		highlightTruthTableRows();
		(new File("bdd")).mkdirs();
//		graphPanel.loadDot(GraphPanel.convertIteToDot(iteLabel.getText(), listener.getInputs().toString().substring(1, listener.getInputs().toString().length() - 1)).reverse().toString()), "bdd/$" + input);
		graphPanel.loadDot(GraphPanel.convertIteToDot(iteLabel.getText(), new StringBuilder(listener.getInputs().toString().substring(1, listener.getInputs().toString().length() - 1)).reverse().toString()), "bdd/$" + input);
	}

	private void highlightTruthTableRows() {
		String truthTableString = truthTableLabel.getText();
		String[] rows = truthTableString.split("\n");

		// Iterate over each row starting from index 2 (excluding header and separator)
		for (int i = 2; i < rows.length; i++) {
			String row = rows[i];

			// Check if the row contains 't' or 'f'
			if (row.contains("T")) {
				rows[i] = row.replaceAll("0", "<font color='green'>0</font>").replaceAll("1", "<font color='green'>1</font>").replaceAll("T", "<font color='green'>T</font>");
			} else if (row.contains("F")) {
				rows[i] = row.replaceAll("0", "<font color='red'>0</font>").replaceAll("1", "<font color='red'>1</font>").replaceAll("F", "<font color='red'>F</font>");
			}
		}

		// Join the modified rows and update the truth table text
		String updatedTruthTableString = String.join("<br>", rows);
		truthTableLabel.setText("<html><pre>" + updatedTruthTableString + "</pre></html>");
	}

	private void copyToClipboard(String text) {
		StringSelection selection = new StringSelection(text);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(selection, null);
	}
}
