/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import org.junit.Test;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class TestOptimisable {

	@Test
	public void test() {
		MyListener l1 = QuantrBooleanAlgebra.parse("o=a(b+b')");
		System.out.println(l1.output.isOptimisable(QuantrBooleanAlgebra.Law.ABSORPTION, QuantrBooleanAlgebra.Law.ASSOCIATIVE, QuantrBooleanAlgebra.Law.COMMUNTATIVE, QuantrBooleanAlgebra.Law.DE_MORGANS, QuantrBooleanAlgebra.Law.DISTRIBUTIVE,
				QuantrBooleanAlgebra.Law.DOUBLE_NEGATIVE, QuantrBooleanAlgebra.Law.IDEMPOTENT, QuantrBooleanAlgebra.Law.IDENTITY, QuantrBooleanAlgebra.Law.NEGATION, QuantrBooleanAlgebra.Law.UNIVERSAL_BOUND));
	}
}
