/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import org.junit.Test;

/**
 *
 * @author Ronald Park <pkc6krt22@gmail.com>
 */
public class TestComparator {

	@Test
	public void test() {
		MyListener l1 = QuantrBooleanAlgebra.parse("o=a+b(c+e)");
		MyListener l2 = QuantrBooleanAlgebra.parse("o=(c+e)b+a");
		System.out.println(l1.output.equals(l2.output));
	}
}
