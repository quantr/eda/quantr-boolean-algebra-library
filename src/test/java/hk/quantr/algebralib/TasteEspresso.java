/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import java.util.Collection;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@RunWith(Parameterized.class)
public class TasteEspresso {

	private String inputExpr;

	public TasteEspresso(String inputExpr) {
		this.inputExpr = inputExpr;
	}

	@Parameterized.Parameters
	public static Collection<String> testCases() {
		Random random = new Random();
		int numCases = 1;
		int maxNumTerms = 5;
		int maxNumVars = 5;
		return IntStream.range(0, numCases)
				.mapToObj(i -> genBoolFunc(maxNumVars, maxNumTerms))
				.collect(Collectors.toList());
	}

	public static String genBoolFunc(int maxNumVars, int maxNumTerms) {
		Random random = new Random();
		int numVars = maxNumVars; //random.nextInt(maxNumVars) + 1;
		int numTerms = maxNumTerms; //random.nextInt(maxNumTerms) + 1;
		StringBuilder sb = new StringBuilder();
		sb.append("o=");
		for (int i = 0; i < numTerms; i++) {
			boolean[] vars = new boolean[numVars];
			for (int j = 0; j < numVars; j++) {
				vars[j] = random.nextBoolean();
			}
			String term = IntStream.range(0, numVars)
					.filter(j -> vars[j])
					.mapToObj(j -> Character.toString((char) ('a' + j)))
					.map(var -> random.nextBoolean() ? var : var + "'")
					.collect(Collectors.joining(""));
			if (!term.equals("")) {
				sb.append(term).append('+');
			}
		}
		sb.deleteCharAt(sb.length() - 1);
		System.out.println(sb.toString());
		return sb.toString();
	}

	@Test
	public void test() {
//		String expr = "o=(AB)'(BC)'((BC)'D)+((AB)'(BC)')'((BC)'D)'";
		MyListener listener = QuantrBooleanAlgebra.parse("o=((AB)'(BC)'+((BC)'D)(CD'))'"); // bcd'+ad'+ad'+a'd'
		System.out.println("\nInput:\n" + listener.output + "=" + listener.output.getExpr());
		String tt1 = QuantrBooleanAlgebra.getTruthTableAsString(listener);
		System.out.println(listener.output.getExpr());
		listener = QuantrBooleanAlgebra.eOptimise(listener);
		System.out.println("Espresso:\n" + listener.output + "=" + listener.output.getExpr());
		assertEquals(tt1, QuantrBooleanAlgebra.getTruthTableAsString(listener));
	}
}
