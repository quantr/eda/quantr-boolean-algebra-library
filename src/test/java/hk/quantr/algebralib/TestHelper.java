/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestHelper {

	@Test
	public void test() {
		String instruction = "o=ab+c1";
		System.out.println(instruction);
//		BooleanData inputs[] = QuantrBooleanAlgebra.getInputs(instruction);
//		Stream.of(inputs).forEach(s -> System.out.println("input: " + s));
//		byte[][] truthTable = QuantrBooleanAlgebra.getTruthTable("o=ab+c");
//		for (int y = 0; y < truthTable.length; y++) {
//			for (int x = 0; x < truthTable[y].length; x++) {
//				System.out.print(truthTable[y][x] + " ");
//			}
//			System.out.println();
//		}
//
//		System.out.println("-".repeat(30));
		QuantrBooleanAlgebra.printTruthTable(QuantrBooleanAlgebra.parse(instruction));
	}
}
