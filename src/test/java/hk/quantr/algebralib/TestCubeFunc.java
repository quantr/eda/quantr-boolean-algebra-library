/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class TestCubeFunc {

	@Test
	public void testCovers() {
		System.out.println("testCovers:");
		Cube cubeA = new Cube(new int[]{3, 1, 3});
		Cube cubeB = new Cube(new int[]{1, 1, 3});
		System.out.println(cubeA.covers(cubeB));
	}

	@Test
	public void testIntersects() {
		System.out.println("testIntersects:");
		Cube cubeA = new Cube(new int[]{3, 1, 2});
		Cube cubeB = new Cube(new int[]{1, 1, 3});
		System.out.println(cubeA.intersects(cubeB));
	}
//
//	@Test
//	public void testCombinable() {
//		System.out.println("testCombinable:");
//		Cube cubeA = new Cube(new int[]{1, 1, 1});
//		Cube cubeB = new Cube(new int[]{1, 1, 2});
//		System.out.println(cubeA.combinable(cubeB));
//	}
//
//	@Test
//	public void testCombine() {
//		System.out.println("testCombine:");
//		Cube cubeA = new Cube(new int[]{1, 1, 1});
//		Cube cubeB = new Cube(new int[]{1, 1, 2});
//		System.out.println(cubeA.combine(cubeB));
//	}

	@Test
	public void testGetMinterms() {
		System.out.println("testGetMinterms:");
		Cube cube = new Cube(new int[]{1, 3, 3});
		System.out.println(cube.split());
	}

	@Test
	public void testCubeClone() {
		System.out.println("testCubeClone:");
		Cube cube = new Cube(new int[]{1, 3, 3});
		Cube clone = cube.copy();
		clone.setLiteralValue(1, 1);
		System.out.println(clone + "/" + cube);
	}

	@Test
	public void testCubeListClone() {
		System.out.println("testCubeListClone:");
		Cube cubeA = new Cube(new int[]{1, 2, 3});
		Cube cubeB = new Cube(new int[]{1, 3, 3});
		CubeList cubeList = new CubeList();
		cubeList.add(cubeA);
		cubeList.add(cubeB);
		CubeList clone = cubeList.copy();
		((Cube) clone.get(0)).setLiteralValue(2, 1);
		System.out.println(cubeList + "/" + clone);
	}
}
