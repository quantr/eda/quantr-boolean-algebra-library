package hk.quantr.algebralib;

import hk.quantr.algebralib.data.BooleanData;
import hk.quantr.algebralib.antlr.BooleanAlgebraLexer;
import hk.quantr.algebralib.antlr.BooleanAlgebraParser;
import java.util.stream.Collectors;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestParser {

	@Test
	public void test() throws Exception {
//		String instruction = "o=b'+a";
//		System.out.println(instruction);
//		BooleanAlgebraLexer lexer = new BooleanAlgebraLexer(CharStreams.fromString(instruction));
//		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//		BooleanAlgebraParser parser = new BooleanAlgebraParser(tokenStream);
//		ParseTree tree = parser.parse();
//		ParseTreeWalker walker = new ParseTreeWalker();
//		MyListener l = new MyListener();
//		walker.walk(l, tree);
		String expr = "o=ab+c";
		MyListener l = QuantrBooleanAlgebra.parse(expr);
		System.out.println(l.output + "=" + l.output.getExpr());
		QuantrBooleanAlgebra.printKMap(l);
	}
}
