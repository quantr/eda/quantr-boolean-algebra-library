/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import hk.quantr.algebralib.data.Output;
import org.junit.Test;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class TestOptimiser {

	@Test
	public void test() {
//		MyListener listener = QuantrBooleanAlgebra.parse("o=(abc'+c'd')(ab'c'd)"); // (AB)'(BC)'((BC)'D)+((AB)'(BC)')'((BC)'D)'
//		System.out.println(listener.output + "=" + listener.output.getExpr());
//		listener.output = (Output) QuantrBooleanAlgebra.baOptimise(listener.output);
//		System.out.println(listener.output + "=" + listener.output.getExpr());
//		QuantrBooleanAlgebra.dump(listener.output);
		MyListener listener = QuantrBooleanAlgebra.parse("o=ab+b'"); // (AB)'(BC)'((BC)'D)+((AB)'(BC)')'((BC)'D)'
		System.out.println(listener.output + "=" + listener.output.getExpr());
		MyListener listener2 = QuantrBooleanAlgebra.baOptimise(listener);
		System.out.println(listener2.output + "=" + listener2.output.getExpr());
	}

	/*
	QuantrBooleanAlgebra.Law.ABSORPTION, QuantrBooleanAlgebra.Law.ASSOCIATIVE, QuantrBooleanAlgebra.Law.COMMUNTATIVE, QuantrBooleanAlgebra.Law.DE_MORGANS, QuantrBooleanAlgebra.Law.DISTRIBUTIVE,
	QuantrBooleanAlgebra.Law.DOUBLE_NEGATIVE, QuantrBooleanAlgebra.Law.IDEMPOTENT, QuantrBooleanAlgebra.Law.IDENTITY, QuantrBooleanAlgebra.Law.NEGATION, QuantrBooleanAlgebra.Law.UNIVERSAL_BOUND
	 */
}
