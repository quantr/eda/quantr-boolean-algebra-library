/*
 * Copyright 2024 Ronald <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.algebralib;

import static hk.quantr.algebralib.QuantrBooleanAlgebra.parseIte;
import org.junit.Test;

/**
 *
 * @author Ronald <pkc6krt22@gmail.com>
 */
public class TestIteListener {

	@Test
	public void test1() {
		IteListener l = parseIte("ITE(a,ITE(b,0,1),0)", "ab");
		System.out.println(l.getDot());
	}
}
